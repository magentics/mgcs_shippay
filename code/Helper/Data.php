<?php

class Mgcs_Shippay_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ACTIVE       = 'shipping/shippay/active';
    const XML_PATH_MODE         = 'shipping/shippay/mode';
    const XML_PATH_COMBINATIONS = 'shipping/shippay/combinations';

    const MODE_DISALLOW = 'disallow';
    const MODE_ALLOW    = 'allow';

    protected $_combinations;


    /**
     * Get an array of combinations
     *
     * An array of associative arrays with keys 'shipping_method' and 'payment_method':
     * array(
     *     array('shipping_method' => 'tablerate_bestway' => 'payment_method' => 'checkmo'),
     * )
     *
     * @param mixed $store  Store model or id
     * @return array
     */
    public function getCombinations($store = null)
    {
        if (is_null($this->_combinations)) {
            $this->_combinations = unserialize(Mage::getStoreConfig(self::XML_PATH_COMBINATIONS, $store));
        }
        return $this->_combinations;
    }


    /**
     * Is the module set active?
     *
     * @param mixed $store  Store model or id
     * @return bool
     */
    public function isActive($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $store);
    }


    /**
     * Get mode from configuration
     *
     * One of self::MODE_DISALLOW, self::MODE_ALLOW
     *
     * @param mixed $store  Store model or id
     * @return string
     */
    public function getMode($store = null)
    {
        // enforce one of the constants
        switch (Mage::getStoreConfig(self::XML_PATH_MODE, $store)) {
            case self::MODE_ALLOW:
                return self::MODE_ALLOW;
                break; // unnecessary, I know :)
        }
        return self::MODE_DISALLOW;
    }


    /**
     * Is a combination allowed?
     *
     * @param string $shippingMethod
     * @param string $paymentMethod
     * @param mixed $store  Store model or id
     * @return bool
     */
    public function isCombinationAllowed($shippingMethod, $paymentMethod, $store = null)
    {
        $carrier = substr($shippingMethod, 0, strpos($shippingMethod, '_'));
        foreach ($this->getCombinations($store) as $combination) {
            if (empty($combination['shipping_method']) || empty($combination['payment_method'])) {
                continue;
            }
            if ($combination['shipping_method'] == $carrier && $combination['payment_method'] == $paymentMethod) {
                return $this->getMode($store) != self::MODE_DISALLOW;
            }
        }
        return $this->getMode($store) == self::MODE_DISALLOW;
    }
}