<?php

/** Derived from Mage_CatalogInventory_Block_Adminhtml_Form_Field_Customergroup (but simplified) */
class Mgcs_Shippay_Block_Adminhtml_System_Config_Form_Field_Renderer_Paymentmethod
    extends Mage_Core_Block_Html_Select
{
    /**
     * Options cache
     *
     * @var array
     */
    private $_optionsCache;

    /**
     * Retrieve options
     *
     * @return array
     */
    protected function _getOptions()
    {
        if (is_null($this->_optionsCache)) {
            // get active only (first parameter = true)
            $methods = Mage::getModel('mgcs_shippay/system_config_source_paymentmethods')->toOptionArray(true);
            foreach ($methods as $key => $method) {
                $methods[$key] = $this->_addCodeToLabel($method, $key);
            }
            $this->_optionsCache = $methods;
        }
        return $this->_optionsCache;
    }

    protected function _addCodeToLabel($option, $code)
    {
        if (isset($option['label']) && !is_array($option['value'])) {
            $option['label'] = '[' . $code . '] ' . addslashes($option['label']);
        }
        if (is_array($option['value'])) {
            foreach ($option['value'] as $code2 => $option2) {
                $option['value'][$code2] = $this->_addCodeToLabel($option2, $code2);
            }
        }
        return $option;
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        $this->setOptions($this->_getOptions());
        return parent::_toHtml();
    }
}
