<?php

/** Derived from Mage_CatalogInventory_Block_Adminhtml_Form_Field_Customergroup (but simplified) */
class Mgcs_Shippay_Block_Adminhtml_System_Config_Form_Field_Renderer_Shippingmethod
    extends Mage_Core_Block_Html_Select
{
    /**
     * Options cache
     *
     * @var array
     */
    private $_optionsCache;

    /**
     * Retrieve options
     *
     * @return array
     */
    protected function _getOptions()
    {
        if (is_null($this->_optionsCache)) {
            // get active only (first parameter = true)
            $this->_optionsCache = Mage::getModel('mgcs_shippay/system_config_source_shippingmethods')->toOptionArray(false);
        }
        return $this->_optionsCache;
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        $this->setOptions($this->_getOptions());
        return parent::_toHtml();
    }
}
