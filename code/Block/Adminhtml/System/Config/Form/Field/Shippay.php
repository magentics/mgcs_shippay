<?php

/** Derived from Mage_CatalogInventory_Block_Adminhtml_Form_Field_Minsaleqty */
class Mgcs_Shippay_Block_Adminhtml_System_Config_Form_Field_Shippay
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_shippingRenderer;
    protected $_paymentRenderer;

    protected function _getShippingRenderer()
    {
        if (is_null($this->_shippingRenderer)) {
            $this->_shippingRenderer = $this->getLayout()->createBlock(
                'mgcs_shippay/adminhtml_system_config_form_field_renderer_shippingmethod', '',
                array('is_render_to_js_template' => true)
            )->setClass('shipping_method_select')->setExtraParams('style="width:200px"');
        }
        return $this->_shippingRenderer;
    }

    protected function _getPaymentRenderer()
    {
        if (is_null($this->_paymentRenderer)) {
            $this->_paymentRenderer = $this->getLayout()->createBlock(
                'mgcs_shippay/adminhtml_system_config_form_field_renderer_paymentmethod', '',
                array('is_render_to_js_template' => true)
            )->setClass('payment_method_select')->setExtraParams('style="width:200px"');
        }
        return $this->_paymentRenderer;
    }

    /**
     * Prepare to render
     */
    protected function _prepareToRender()
    {
        $this->addColumn('shipping_method', array(
            'label' => Mage::helper('mgcs_shippay')->__('Shipping Method'),
            'renderer' => $this->_getShippingRenderer(),
        ));
        $this->addColumn('payment_method', array(
            'label' => Mage::helper('mgcs_shippay')->__('Payment Method'),
            'renderer' => $this->_getPaymentRenderer(),
        ));
        $this->_addAfter = false;
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getShippingRenderer()->calcOptionHash($row->getData('shipping_method')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getPaymentRenderer()->calcOptionHash($row->getData('payment_method')),
            'selected="selected"'
        );
    }

}
