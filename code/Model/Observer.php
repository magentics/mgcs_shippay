<?php

class Mgcs_Shippay_Model_Observer
{

    /**
     * Is a payment method available?
     *
     * @event payment_method_is_active
     * @return void
     */
    public function isPaymentMethodAvailable(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('mgcs_shippay');
        if (!$helper->isActive()) {
            return;
        }

        /** @var Mage_Payment_Model_Method_Abstract */
        $method = $observer->getEvent()->getMethodInstance();
        /** @var Mage_Sales_Model_Quote */
        $quote  = $observer->getEvent()->getQuote();
        /** @var StdClass */
        $result = $observer->getEvent()->getResult();

        if ($quote && $quote->getShippingAddress()) {
            $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
            $paymentMethod  = $method->getCode();

            if (!Mage::helper('mgcs_shippay')->isCombinationAllowed($shippingMethod, $paymentMethod)) {
                $result->isAvailable = false;
            }
        }
    }
}