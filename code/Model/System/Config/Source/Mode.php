<?php

class Mgcs_Shippay_Model_System_Config_Source_Mode
{

    public function toOptionArray()
    {
        return array(
            array(
                'value' => Mgcs_Shippay_Helper_Data::MODE_DISALLOW,
                'label' => Mage::helper('mgcs_shippay')->__('Disallow Combinations'),
            ),
            array(
                'value' => Mgcs_Shippay_Helper_Data::MODE_ALLOW,
                'label' => Mage::helper('mgcs_shippay')->__('Allow Combinations'),
            ),
        );
    }

}
