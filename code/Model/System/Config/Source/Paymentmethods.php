<?php

class Mgcs_Shippay_Model_System_Config_Source_Paymentmethods
    extends Mage_Adminhtml_Model_System_Config_Source_Payment_Allmethods
{

    public function toOptionArray()
    {
        $methods = Mage::helper('payment')->getPaymentMethodList(true, true, true);
        foreach ($methods as $key => $method) {
            if ($method['label'] == '') {
                // you can't even see them in the frontend
                unset($methods[$key]);
            }
        }
        return $methods;
    }

}
